import {routing} from "./routes";
import {BrowserModule} from "@angular/platform-browser";
import {ApplicationContainer} from "./containers/application/application.container";
import {NgModule} from "@angular/core";
import {AppSandbox} from "./app.sandbox";
import {AboutModule} from "../about/index";
import {ContactsModule} from "../contacts/index";
import {AuthModule} from "../auth/index";
import {rootReducer} from "../state/rootReducer";
import {StoreModule} from "@ngrx/store";
import {AuthenticatedGuard} from "../auth/guards/authenticated.guard";
import {HTTP_PROVIDER} from "../common/http/HttpDecorator";
import {NavigationComponent} from "../common/components/navigation/navigation.component";
import {SharedModule} from "../common/index";
@NgModule({
    imports: [BrowserModule, AboutModule, ContactsModule, AuthModule,
        SharedModule, StoreModule.provideStore(rootReducer), routing],
    declarations: [ApplicationContainer, NavigationComponent],
    bootstrap: [ApplicationContainer],
    providers: [
        AppSandbox,
        AuthenticatedGuard,
        HTTP_PROVIDER
    ]
})
export class AppModule {
}