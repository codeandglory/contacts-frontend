import {Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy} from "@angular/core";
import {Contact} from "../../domain/Contact";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {EmailValidator} from "../../validators/email.validator";
@Component({
    selector: "contact-detail",
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: require("./contact-detail.component.html"),
    styles: [require("./contact-detail.component.scss")]
})
export class ContactDetailComponent implements OnInit {
    @Input() contact: Contact;
    @Output() onSave: EventEmitter<Contact> = new EventEmitter<Contact>();
    @Output() onDelete: EventEmitter<Contact> = new EventEmitter<Contact>();
    @Output() onCancel: EventEmitter<Contact> = new EventEmitter<any>();

    private contactForm: FormGroup;

    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.contactForm = this.formBuilder.group({
           name: [this.contact.name, Validators.required],
            email: [this.contact.email, EmailValidator.validate]
        });
    }

    save(): void {
        let contact = Object.assign(this.contact, this.contactForm.value);

        if (this.contact.id) {
            contact.id = this.contact.id;
        }

        this.onSave.emit(contact);
    }

    delete(): void {
        this.onDelete.emit(this.contact);
    }

    cancel(): void {
        this.onCancel.emit(null);
    }
}