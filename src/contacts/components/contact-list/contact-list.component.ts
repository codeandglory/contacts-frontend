import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Contact} from "../../domain/Contact";
@Component({
    selector: "contact-list",
    template: `
        <ul class="contacts-list">
            <li *ngFor="let contact of contacts" (click)="openContact(contact)">
                <contact [contact]="contact"></contact>                
            </li>
        </ul>
    `,
    styles: [require("./contact-list.component.scss")]
})
export class ContactListComponent {
    @Input() contacts: Contact[];
    @Output() onOpen: EventEmitter<Contact> = new EventEmitter<Contact>();

    public openContact(contact: Contact): void {
        this.onOpen.emit(contact);
    }
}