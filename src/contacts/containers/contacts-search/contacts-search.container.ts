import {Component, OnInit} from "@angular/core";
import {ContactsSandbox} from "../../contacts.sandbox";
import {Contact} from "../../domain/Contact";
import {Observable} from "rxjs";
import {FormControl} from "@angular/forms";
import {Router} from "@angular/router";
@Component({
    selector: "contacts-search-container",
    styles: [require("./contacts-search.container.scss")],
    template: `    
        <div class="container">
            <div class="contacts-search">
                <input class="contacts-search__input" type="text" [formControl]="searchCtrl" 
                    placeholder="{{'placeholder.search' | translate}}"/>            
                <i class="fa fa-search" aria-hidden="true"></i>
            </div>
            <div class="add-new-contact">
                <a class="add-new-contact__link" [routerLink]="['/contacts/add']">
                    <i class="fa fa-plus-circle"></i>&nbsp;{{'label.contact.new' | translate}}
                </a>
            </div>
            <contact-list [contacts]="matchingContacts | async"
                (onOpen)="openContact($event)"
                (onRemove)="removeContact($event)"></contact-list>
        </div>
     `
})
export class ContactsSearchContainer implements OnInit {

    private searchCtrl = new FormControl("");
    private matchingContacts: Observable<Contact[]>;

    constructor(private sandbox: ContactsSandbox, private router: Router) {

    }

    ngOnInit(): void {
        this.matchingContacts = Observable.combineLatest(
            this.searchCtrl.valueChanges.startWith(""),
            this.sandbox.contacts,
            (searchTerm: string, contacts: Contact[]) => {
                return contacts.filter(contact => contact.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1);
            }
        );
    }

    public openContact(contact: Contact): void {
        this.router.navigate([`/contacts/edit/${contact.id}`]);
    }
}
