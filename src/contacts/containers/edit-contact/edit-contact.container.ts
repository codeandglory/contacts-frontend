import {Component, OnDestroy} from "@angular/core";
import {Contact} from "../../domain/Contact";
import {ActivatedRoute, Router} from "@angular/router";
import {ContactsSandbox} from "../../contacts.sandbox";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs";
@Component({
    selector: "contact-edit",
    template: `
        <div class="container">
            <contact-detail *ngIf="(contact | async)" 
                [contact]="contact | async"
                (onDelete)="deleteContact($event)"
                (onSave)="updateContact($event)"
                (onCancel)="navigateToContacts()">                
            </contact-detail>
        </div>
    `
})
export class EditContactContainer implements OnDestroy {

    private subscriptions: Subscription[] = [];

    private contact: Observable<Contact>;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private contactsSandbox: ContactsSandbox) {
        this.contact = this.contactsSandbox.getContact(this.route.snapshot.params["id"]).publishLast().refCount();
    }

    public deleteContact(contact: Contact): void {
        let sc: Subscription = this.contactsSandbox.deleteContact(contact.id)
            .subscribe(() => this.navigateToContacts());
        this.subscriptions.push(sc);
    }

    public updateContact(contact: Contact): void {
        let sc: Subscription = this.contactsSandbox.updateContact(contact)
            .subscribe(() => this.navigateToContacts());
        this.subscriptions.push(sc);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    private navigateToContacts() {
        this.router.navigate(["/contacts"]);
    }

}