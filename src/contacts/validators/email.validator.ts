import {FormControl} from "@angular/forms";
export class EmailValidator {

    private static EMAIL_REGEXP: RegExp = /^[^@]+@[^@]+\.[^@]+$/;

    public static validate(formControl: FormControl): {
        [key: string]: any;
    } {
        return EmailValidator.EMAIL_REGEXP.test(formControl.value) ? null : {
            validateEmail: {
                valid: false
            }
        };
    }

}