import {
    Http, Request, RequestOptionsArgs, Response, ConnectionBackend, RequestOptions,
    XHRBackend, Headers
} from "@angular/http";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {ApplicationState} from "../../state/states/ApplicationState";
import {Store} from "@ngrx/store";
import {ActionCreator} from "../../state/ActionCreator";
import {LOCALSTORAGE_JWT_KEY} from "../../configuration";
export class HttpDecorator extends Http {

    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions,
                private router: Router, private store: Store<ApplicationState>) {
        super(backend, defaultOptions);
    }

    request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.request(url, this.authRequestOptions(options)));
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.get(url, this.authRequestOptions(options)));
    }

    post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.post(url, body, this.authRequestOptions(options)));
    }

    put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.put(url, body, this.authRequestOptions(options)));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.delete(url, this.authRequestOptions(options)));
    }

    patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.patch(url, body, this.authRequestOptions(options)));
    }

    head(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.head(url, this.authRequestOptions(options)));
    }

    options(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.options(url, options));
    }

    private authRequestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
        let newOptions = options || {headers: new Headers()};

        let token = localStorage.getItem(LOCALSTORAGE_JWT_KEY);
        console.log("SETTING TOKEN", token);
        if (token) {
            newOptions.headers.set("Authorization", `Bearer ${token}`);
        }

        return newOptions;
    }

    private intercept(observable: Observable<Response>): Observable<Response> {
        return observable.catch(err => {
            console.log(err);
            if (err.status  === 401) {
                this.store.dispatch(ActionCreator.clearAuthentication());
                this.router.navigate(["/login"]);
                return Observable.empty();
            } else {
                return Observable.throw(err);
            }
        });

    }
}

export const HTTP_PROVIDER: any = {
    provide: Http,
    useFactory: (xhrBackend: XHRBackend, requestOptions: RequestOptions, router: Router, store: Store<ApplicationState>) => {
        return new HttpDecorator(xhrBackend, requestOptions, router, store);
    },
    deps: [XHRBackend, RequestOptions, Router, Store]
};