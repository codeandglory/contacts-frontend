import {TranslateLoader} from "ng2-translate";
import {Observable} from "rxjs";
export class CustomTranslateLoader implements TranslateLoader {

    getTranslation(lang: string): Observable<any> {
        console.log(`../i18n/${lang}.json`);
        return Observable.of(require(`./${lang}.json`));
    }

}