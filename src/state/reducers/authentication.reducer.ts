import {Action} from "@ngrx/store";
import {AuthenticationDataState} from "../states/AuthenticationDataState";
import {ActionTypes} from "../ActionTypes";
import {Account} from "../../common/domain/Account";

let initialState: AuthenticationDataState = {
    isAuthenticated: false,
    username: null,
    token: null
};

export function authenticationReducer(state: AuthenticationDataState = initialState,
                                      action: Action = null): AuthenticationDataState {
    switch (action.type) {
        case ActionTypes.SET_AUTHENTICATION:
            let account: Account = action.payload;
            return {
                isAuthenticated: true,
                username: account.username,
                token: account.token
            };
        case ActionTypes.CLEAR_AUTHENTICATION:
            return {
                isAuthenticated: false,
                username: null,
                token: null
            };
        default:
            return state;
    }
}