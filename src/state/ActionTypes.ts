export class ActionTypes {
    public static SET_AUTHENTICATION: string = "SET_AUTHENTICATION";
    public static CLEAR_AUTHENTICATION: string = "CLEAR_AUTHENTICATION";
}