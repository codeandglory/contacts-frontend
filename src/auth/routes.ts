import {RouterModule} from "@angular/router";
import {AuthContainer} from "./containers/auth.container";
export const routes = [
    { path: "login", component: AuthContainer}
];

export const routing = RouterModule.forChild(routes);
