import {Component, OnDestroy} from "@angular/core";
import {AuthSandbox} from "../auth.sandbox";
import {Credentials} from "../domain/Credentials";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
    selector: "auth-container",
    styles: [require("./auth.container.scss")],
    template: `
            <div class="auth-container">
               <login *ngIf="showLogin"
                    (onLogin)="onLogin($event)"
                    (onLogout)="onLogout()"
                    (onRegister)="toggleLogin()"
                    [ngClass]="{'error': error}"
               ></login>
               <register *ngIf="!showLogin"                    
                    (onRegister)="onRegister($event)"
                    (onCancel)="toggleLogin()"
                    [ngClass]="{'error': error}"
               ></register>
            </div>
        `
})
export class AuthContainer implements OnDestroy {

    public showLogin: boolean = true;
    public error: boolean = false;
    private subscriptions: Subscription[] = [];

    constructor(private authSandbox: AuthSandbox, private router: Router) {
    }

    public onLogin(credentials: Credentials): void {
        let sc = this.authSandbox.login(credentials).subscribe(() => {
            this.router.navigate(["/"]);
        }, () => {
            this.error = true;
        });
        this.subscriptions.push(sc);
    }

    public onLogout(): void {
        this.authSandbox.logout();
    }

    public onRegister(credentials: Credentials): void {
        let sc = this.authSandbox.register(credentials).subscribe(() => {
            this.onLogin(credentials);
        }, () => {
            this.error = true;
        });
        this.subscriptions.push(sc);
    }

    public toggleLogin(): void {
        this.error = false;
        this.showLogin = !this.showLogin;
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
}