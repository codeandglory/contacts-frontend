import {Credentials} from "../domain/Credentials";
import {Http, Response} from "@angular/http";
import {API_URL, LOCALSTORAGE_JWT_KEY} from "../../configuration";
import {Observable} from "rxjs";
import {Account} from "../../common/domain/Account";
import {Store} from "@ngrx/store";
import {ActionCreator} from "../../state/ActionCreator";
import {ApplicationState} from "../../state/states/ApplicationState";
import * as decodeJwt from "jwt-decode";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthenticationService {

    constructor(private http: Http, private store: Store<ApplicationState>) {

    }

    public authenticate(credentials: Credentials): Observable<Account> {
        let obs$: Observable<Account> = this.http.post(`${API_URL}/auth`, credentials)
            .map((response: Response) => response.json());

        obs$.subscribe((account: Account) => {
            localStorage.setItem(LOCALSTORAGE_JWT_KEY, account.token);
            this.store.dispatch(ActionCreator.setAuthentication(account));
        });

        return obs$;
    }

    public logout(): void {
        localStorage.removeItem(LOCALSTORAGE_JWT_KEY);
        this.store.dispatch(ActionCreator.clearAuthentication());
    }

    public restoreAuthentication(): void {
        if (this.isValidAuthentication()) {
            let token = localStorage.getItem(LOCALSTORAGE_JWT_KEY);
            var decoded: any = decodeJwt(token);
            let account: Account = new Account(decoded.username, token);
            this.store.dispatch(ActionCreator.setAuthentication(account));
        }
    }

    public isValidAuthentication(): boolean {
        return !!localStorage.getItem(LOCALSTORAGE_JWT_KEY);
    }

    public register(credentials: Credentials): Observable<any> {
        return this.http.post(`${API_URL}/auth/register`, credentials);
    }
}