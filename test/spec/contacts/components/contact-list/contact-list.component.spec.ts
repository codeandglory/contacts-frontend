import {async, TestBed, ComponentFixture} from "@angular/core/testing";
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {Contact} from "../../../../../src/contacts/domain/Contact";
import {ContactListComponent} from "../../../../../src/contacts/components/contact-list/contact-list.component";
describe("Test: Contact List component", () => {

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ContactListComponent],
            schemas: [NO_ERRORS_SCHEMA]
        });
        TestBed.compileComponents();
    }));

    it("Should trigger the correct open event", () => {
        const fixture: ComponentFixture<ContactListComponent> = TestBed.createComponent(ContactListComponent);
        const element = fixture.nativeElement;
        const contactList: Contact[] = [new Contact(0, "Foo"), new Contact(1, "Bar")];
        const component = fixture.componentInstance;
        component.contacts = contactList;
        spyOn(component.onOpen, "emit");

        fixture.detectChanges();

        expect(element.querySelectorAll("li").length).toEqual(2);
        element.querySelectorAll("li")[0].click();
        fixture.detectChanges();

        expect(component.onOpen.emit).toHaveBeenCalledWith(contactList[0]);
    });
});